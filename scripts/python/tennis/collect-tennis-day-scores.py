#!/usr/bin/env python 
# coding: utf-8

from bs4 import BeautifulSoup
import urllib2
import sys

base_url='http://m.flashresultats.fr'
api_url=''
if len(sys.argv) == 2:
    api_url=base_url+'/tennis?d='+sys.argv[1]
else:
    api_url=base_url+'/tennis'
sock = urllib2.urlopen(api_url)

html_doc = sock.read()
sock.close()
soup = BeautifulSoup(html_doc, "html.parser")

scores = soup.find(id='score-data')

tournament=''
time=''
playersOptionalPart1=''
playersMandatoryPart2=''
playersOptionalPart3=''
playersOptionalPart4=''
link=''
score=''
t=0
m=0
print '{"tournaments": ['
for child in scores.children:
    # reset each tournament
    if child.name == 'h4':
        tournament=child.string.encode('utf-8')
        time=''
        playersOptionalPart1=''
        playersMandatoryPart2=''
        playersOptionalPart3=''
        playersOptionalPart4=''
        link=''
        score=''
        if t>0:
            print ']},'
        print '{'
        print '"name": "%s",' % tournament
        print '"matches": ['
        t=t+1
        m=0
    elif child.name == 'br':
        time=''
        playersOptionalPart1=''
        playersMandatoryPart2=''
        playersOptionalPart3=''
        playersOptionalPart4=''
        link=''
        score=''
    elif time == '':
        time = child.get_text(' - ', strip=True).encode('utf-8')
    elif child.name == 'img':
        if playersMandatoryPart2 == '':
            playersOptionalPart1 = '[serving] '
        else:
            playersOptionalPart3 = '[serving] '
    elif playersMandatoryPart2 == '':
        playersMandatoryPart2 = child.string.encode('utf-8')
    elif playersOptionalPart4 == '' and playersOptionalPart3 != '':
        playersOptionalPart4 = child.string.encode('utf-8')
    elif score == '':
        link = base_url + child.get('href').encode('utf-8')
        score = child.string
        if m>0:
            print ','
        print '{'
        print '"time": "%s",' % time
        print '"players": "%s",' % (playersMandatoryPart2 + playersOptionalPart4)
        print '"playersWithCurrentServer": "%s",' % (playersOptionalPart1 + playersMandatoryPart2 + playersOptionalPart3 + playersOptionalPart4)
        print '"link": "%s",' % link
        print '"score": "%s"' % score
        print '}'
        m=m+1
if t>0:
    print ']}'
print ']}'
