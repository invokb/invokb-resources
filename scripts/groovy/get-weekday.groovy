day=args[0].toInteger()
monthName=args[1]
year=args[2].toInteger()

monthNames = [ "january", "february", "march", "april", "may", "june", "july", "august",
  "september", "october", "november", "december" ]

weekdayNames = [ "", "sunday", "monday", "tuesday", "wednesday", "thursday", "friday",
  "saturday" ]

monthfound = false
for (i = 0; i < monthNames.size(); i++) {
  if (monthNames[i].equalsIgnoreCase(monthName)) {
    calendar = Calendar.getInstance()
    calendar.set(year, i, day)
    weekDay = calendar.get(Calendar.DAY_OF_WEEK)
    println weekdayNames[weekDay]
    monthfound = true
    break
  }
}
if (!monthfound) {
  throw new IllegalArgumentException("The month '" + monthName + "' is invalid.")
}

