import io.vertx.core.AbstractVerticle
import io.vertx.core.eventbus.Message
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject

import java.time.DateTimeException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.time.temporal.ChronoUnit
import java.util.stream.Collectors
import java.util.stream.IntStream
import java.util.stream.Stream

class DateVerticle extends AbstractVerticle {

    private final static DateTimeFormatter ISO_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    @Override
    void start() {
        vertx.eventBus().consumer("date", { Message<JsonObject> message ->
            try {
                final String filter = message.body().getString("filter")
                final String from = toSafeString(message.body().getValue("from"))
                final String to =  toSafeString(message.body().getValue("to"))
                final String mockedTime = toSafeString(message.body().getValue("mockedTime"))
                final LocalDate today = mockedTime == null ? LocalDate.now()
                    : LocalDate.parse(mockedTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                final DateService service = new DateService(today)
                final NamedDate fromNd = service.convertToNamedDate(from)
                final NamedDate toNd = service.convertToNamedDate(to)
                final List<NamedDate> response = service.getNamedDates(filter)
                        .filter({ NamedDate nd -> fromNd == null ||
                            (nd.dayOffset != null && nd.dayOffset >= fromNd.dayOffset) ||
                            (nd.dayOffset == null && service.compareNamedDateNames(nd, fromNd) >= 0)})
                        .filter({ NamedDate nd -> toNd == null  ||
                            (nd.dayOffset != null && nd.dayOffset <= toNd.dayOffset) ||
                            (nd.dayOffset == null && service.compareNamedDateNames(nd, toNd) <= 0)})
                        .collect(Collectors.toList())
                message.reply(Json.encode(response))
            } catch (Exception e) {
                message.fail(123, e.getMessage())
            }
        })
    }

    private String toSafeString(Object value) {
        return value == null ? null : value.toString();
    }

    static final class DateService {

        private final static String DASH = "-"
        private final static String PARTIAL_SUFFIX = "..."

        private final LocalDate today;

        DateService(LocalDate today) {
            this.today = today;
        }

        int compareNamedDateNames(NamedDate candidateNd, NamedDate referenceNd) {
            String candidateNdIsoDateNameStart = extractNameWithoutSuffix(candidateNd)
            String referenceNdIsoDateNameStart = referenceNd.isoDateString.substring(0, candidateNdIsoDateNameStart.length())
            return candidateNdIsoDateNameStart.numberAwareCompareTo(referenceNdIsoDateNameStart)
        }

        String extractNameWithoutSuffix(NamedDate nd) {
            return nd.name.replace(DateService.PARTIAL_SUFFIX, "")
        }

        NamedDate convertToNamedDate(String value) {
            if (!value) {
                return null
            }
            if (value.isInteger()) {
                LocalDate date = today.plusDays(value.toInteger())
                return new NamedDate(date.format(ISO_DATE_FORMATTER), date, today)
            }
            return Stream.concat(getAliasesStream(), getDaysAgoStream())
                    .filter({ nd -> nd.name.equalsIgnoreCase(value) })
                    .findAny()
                    .orElseGet({ new NamedDate(value, LocalDate.parse(value, ISO_DATE_FORMATTER), today) })
        }

        Stream<NamedDate> getNamedDates(String start) {
            Stream<NamedDate> aliasesStream = getAliasesStream()
            Stream<NamedDate> daysAgoStream = getDaysAgoStream()
            Stream<NamedDate> isoDatesStream = getIsoDatesStream(start)
            Stream<NamedDate> stream = Stream.concat(Stream.concat(aliasesStream, daysAgoStream), isoDatesStream)
            return stream.filter({ d -> d.getName().toLowerCase().startsWith(start.toLowerCase()) })
        }

        Stream<NamedDate> getAliasesStream() {
            return Stream.concat(
                    Stream.of(
                            new NamedDate("today", today, today),
                            new NamedDate("tomorrow", today.plusDays(1), today),
                            new NamedDate("yesterday", today.minusDays(1), today)),
                    Stream.concat(
                            IntStream.rangeClosed(2, 7)
                                    .mapToObj({ increment -> today.plusDays(increment) })
                                    .map({ date -> new NamedDate("next " + date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH), date, today) }),
                            IntStream.rangeClosed(2, 7)
                                    .mapToObj({ decrement -> today.minusDays(decrement) })
                                    .map({ date -> new NamedDate("last " + date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH), date, today) })
                    )
            )
        }

        Stream<NamedDate> getDaysAgoStream() {
            return IntStream.rangeClosed(2, 9)
                    .mapToObj({ day -> new NamedDate(day + " days ago", today.minusDays(day), today) })
        }

        Stream<NamedDate> getIsoDatesStream(String start) {
            // complete the year
            if (start.matches('^\\d{0,1}$')) {
                return Stream.of(20, 19).map({ startOfYear -> toPartialNamedDate("" + startOfYear) })
            } else if (start.matches('^\\d{2,3}$')) {
                final int lowerYear
                final int upperYear
                if (start == "20") {
                    int currentYear = today.getYear()
                    lowerYear = 2000
                    upperYear = currentYear + 2 // 2 years in the future
                } else if (start.length() == 2) {
                    int startOfYear = Integer.parseInt(start)
                    lowerYear = startOfYear * 100 // will be 1900 if 19
                    upperYear = lowerYear + 99 // will be 1999 if 19
                } else {
                    int startOfYear = Integer.parseInt(start)
                    lowerYear = startOfYear * 10 // will be 1900 if 199
                    upperYear = lowerYear + 9 // will be 1999 if 199
                }
                return toIntStreamDescendingSequence(upperYear, lowerYear)
                        .mapToObj({ year -> toPartialNamedDate(String.valueOf(year) + DASH) })
            } else if (start.matches('^\\d{4}$') || start.matches('^\\d{4}' + DASH + '\\d?$')) {
                return toIntStreamDescendingSequence(12, 1).mapToObj({ month ->
                    toPartialNamedDate(
                            start.substring(0, 4) + DASH + (month < 10 ? "0" + month : month) + DASH)
                })
            } else if (start.matches('^\\d{4}' + DASH + '\\d{2}')
                    || start.matches('^\\d{4}' + DASH + '\\d{2}' + DASH + '\\d{0,2}$')) {
                int year = Integer.parseInt(start.substring(0, 4))
                int month = Integer.parseInt(start.substring(5, 7))
                try {
                    int lowerDay = 1
                    LocalDate firstDayOfMonth = LocalDate.of(year, month, lowerDay)
                    int upperDay = firstDayOfMonth.lengthOfMonth()
                    return toIntStreamDescendingSequence(upperDay, lowerDay)
                            .mapToObj({ day ->
                        new NamedDate(start.substring(0, 7) + DASH + (day < 10 ? "0" + day : day),
                                LocalDate.of(year, month, day), today)
                    })
                } catch (DateTimeException e) {
                    // ignore the exception
                }
            }
            return Stream.of()
        }

        private IntStream toIntStreamDescendingSequence(int from, int to) {
            return IntStream.rangeClosed(to, from).map({ i -> -i + from + to })
        }

        private NamedDate toPartialNamedDate(String name) {
            return new NamedDate(name + PARTIAL_SUFFIX, null, today)
        }

    }

    static final class NamedDate {

        private final String name
        private final LocalDate date
        private final LocalDate today

        NamedDate(String name, LocalDate date, LocalDate today) {
            super()
            this.name = name
            this.date = date
            this.today = today
        }

        String getName() {
            return name
        }

        int getYear() {
            return date == null ? 0 : date.getYear()
        }

        int getMonth() {
            return date == null ? 0 : date.getMonthValue()
        }

        String getMonthName() {
            return date == null ? null : date.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH)
        }

        int getDay() {
            return date == null ? 0 : date.getDayOfMonth()
        }

        int getWeekday() {
            return date == null ? 0 : date.getDayOfWeek().getValue()
        }

        String getWeekdayName() {
            return date == null ? null : date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH)
        }

        String getIsoDateString() {
            return date == null ? null : date.format(ISO_DATE_FORMATTER)
        }

        Integer getDayOffset() {
            return date == null ? null : ChronoUnit.DAYS.between(today, date)
        }

    }
}
